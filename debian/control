Source: python-networkx
Section: python
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Uploaders: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>,
Build-Depends: debhelper-compat (= 9),
               dh-python,
	       latexmk,
               python-all,
               python-setuptools,
               python-sphinx,
               python3-all,
               python3-setuptools,
Build-Depends-Indep: dvipng,
                     python-ipykernel,
                     python-decorator (>= 4.3.0),
                     python3-decorator (>= 4.3.0),
                     python-gdal,
                     python-matplotlib,
                     python-nb2plots,
                     python-nbformat,
                     python-nbconvert,
                     python-nose,
                     python-numpy,
                     python-numpydoc,
                     python-pydot,
                     python-pygraphviz,
                     python-scipy,
                     python-sphinx-rtd-theme,
                     python-sphinx-gallery,
                     python-traitlets,
                     python3-gdal,
                     python3-matplotlib,
                     python3-nose,
                     python3-numpy,
                     python3-scipy,
                     python3-pygraphviz,
                     python3-pydot,
                     texlive-binaries,
                     texlive-fonts-recommended,
                     texlive-latex-base,
                     texlive-latex-extra,
                     texlive-latex-recommended,
                     texlive-generic-extra,
                     zip,
Standards-Version: 4.2.1
Homepage: http://networkx.github.io/
Vcs-Git: https://salsa.debian.org/python-team/modules/python-networkx.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/python-networkx

Package: python-networkx
Architecture: all
Depends: ${misc:Depends},
         ${python:Depends},
         python-pkg-resources,
Recommends: python-gdal,
            python-matplotlib,
            python-numpy,
            python-pygraphviz | python-pydot,
            python-scipy,
            python-yaml,
Suggests: python-networkx-doc
Description: tool to create, manipulate and study complex networks
 NetworkX is a Python-based package for the creation, manipulation, and
 study of the structure, dynamics, and functions of complex networks.
 .
 The structure of a graph or network is encoded in the edges (connections,
 links, ties, arcs, bonds) between nodes (vertices, sites, actors). If
 unqualified, by graph it's meant a simple undirected graph, i.e. no
 self-loops and no multiple edges are allowed. By a network it's usually
 meant a graph with weights (fields, properties) on nodes and/or edges.
 .
 The potential audience for NetworkX includes: mathematicians, physicists,
 biologists, computer scientists, social scientists.

Package: python3-networkx
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-pkg-resources,
Recommends: python3-gdal,
            python3-numpy,
            python3-scipy,
            python3-yaml,
Suggests: python-networkx-doc
Description: tool to create, manipulate and study complex networks (Python3)
 NetworkX is a Python-based package for the creation, manipulation, and
 study of the structure, dynamics, and functions of complex networks.
 .
 The structure of a graph or network is encoded in the edges (connections,
 links, ties, arcs, bonds) between nodes (vertices, sites, actors). If
 unqualified, by graph it's meant a simple undirected graph, i.e. no
 self-loops and no multiple edges are allowed. By a network it's usually
 meant a graph with weights (fields, properties) on nodes and/or edges.
 .
 The potential audience for NetworkX includes: mathematicians, physicists,
 biologists, computer scientists, social scientists.
 .
 This package contains the Python 3 version of NetworkX.

Package: python-networkx-doc
Section: doc
Architecture: all
Depends: libjs-mathjax,
         ${misc:Depends},
         ${sphinxdoc:Depends},
Description: tool to create, manipulate and study complex networks - documentation
 NetworkX is a Python-based package for the creation, manipulation, and
 study of the structure, dynamics, and functions of complex networks.
 .
 The structure of a graph or network is encoded in the edges (connections,
 links, ties, arcs, bonds) between nodes (vertices, sites, actors). If
 unqualified, by graph it's meant a simple undirected graph, i.e. no
 self-loops and no multiple edges are allowed. By a network it's usually
 meant a graph with weights (fields, properties) on nodes and/or edges.
 .
 The potential audience for NetworkX includes: mathematicians, physicists,
 biologists, computer scientists, social scientists.
 .
 This package contains documentation for NetworkX.
